package com.danizz.Part_1_1;

/**
 * The first Mail class in this course
 * @author Daniil Nizovkin
 * @version 1.0
 */
public class Main {

    /**
     * This method prints "Hello World" on command line
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
