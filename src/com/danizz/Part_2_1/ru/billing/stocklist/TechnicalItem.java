package com.danizz.Part_2_1.ru.billing.stocklist;

public class TechnicalItem extends GenericItem {

    private short warrantyTime;

    public short getWarrantyTime() {
        return warrantyTime;
    }

    public void setWarrantyTime(short warrantyTime) {
        this.warrantyTime = warrantyTime;
    }

    @Override
    public void printAll() {
        super.printAll();
        System.out.println("Warranty time: " + warrantyTime);
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        TechnicalItem that = (TechnicalItem) o;
        return warrantyTime == that.warrantyTime;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) warrantyTime;
        return result;
    }

    @Override
    public String toString() {
        return "TechnicalItem{" +
                "warrantyTime=" + warrantyTime +
                super.toString() +
                '}';
    }
}
