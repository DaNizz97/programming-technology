package com.danizz.Part_2_1.ru.billing.stocklist;

public enum Category {
    FOOD, PRINT, DRESS, GENERAL;
}
