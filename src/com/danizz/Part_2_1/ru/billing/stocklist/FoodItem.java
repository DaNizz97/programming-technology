package com.danizz.Part_2_1.ru.billing.stocklist;

import java.util.Date;
import java.util.Objects;

public class FoodItem extends GenericItem {

    private Date dateOfIncome;
    private short expires;

    public FoodItem(String name, float price, GenericItem analogue, Date dateOfIncome, short expires) {
        super(name, price, analogue);
        this.dateOfIncome = dateOfIncome;
        this.expires = expires;
    }

    public FoodItem(String name, float price, short expires) {
        this(name, price, null, null, expires);
    }

    public FoodItem(String name) {
        this(name, 0F, null, null, (short) 1);
    }

    public Date getDateOfIncome() {
        return dateOfIncome;
    }

    public void setDateOfIncome(Date dateOfIncome) {
        this.dateOfIncome = dateOfIncome;
    }

    public short getExpires() {
        return expires;
    }

    public void setExpires(short expires) {
        this.expires = expires;
    }

    @Override
    public void printAll() {
        super.printAll();
        System.out.println("Date of Income: " + dateOfIncome);
        System.out.println("Experes: " + expires);
    }

    @Override
    public boolean equals(Object otherObject) {
        if (!super.equals(otherObject)) {
            return false;
        }

        FoodItem other = (FoodItem) otherObject;
        return Objects.equals(dateOfIncome, other.dateOfIncome) && Objects.equals(expires, other.expires);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dateOfIncome, expires);
    }

    @Override
    public String toString() {
        return "FoodItem{" +
                "dateOfIncome=" + dateOfIncome +
                ", expires=" + expires +
                super.toString() +
                '}';
    }
}
