package com.danizz.Part_2_1.ru.billing.stocklist;

import java.util.Objects;

public class GenericItem implements Cloneable {

    private int ID;
    private String name;
    private float price;
    private GenericItem analogue;
    private Category category = Category.GENERAL;

    public static int currentID;

    public GenericItem(String name, float price, Category category) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.ID = GenericItem.currentID++;
    }

    public GenericItem(String name, float price, GenericItem analogue) {
        this.name = name;
        this.price = price;
        this.analogue = analogue;
        this.ID = GenericItem.currentID++;
    }

    public GenericItem() {
        this.ID = GenericItem.currentID++;
    }

    public void printAll() {
        System.out.println("ID: " + ID);
        System.out.println("Name: " + name);
        System.out.println("Price: " + price);
        System.out.println("Category: " + category);
    }

    public void setAnalogue(GenericItem analogue) {
        if (ID == analogue.ID)
            this.analogue = analogue;
        else throw new IllegalArgumentException();
    }

    public GenericItem getAnalogue() {
        return analogue;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static int getCurrentID() {
        return currentID;
    }

    public static void setCurrentID(int currentID) {
        GenericItem.currentID = currentID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenericItem that = (GenericItem) o;
        return ID == that.ID &&
                Float.compare(that.price, price) == 0 &&
                Objects.equals(name, that.name) &&
                category == that.category;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, name, price, category);
    }

    @Override
    public GenericItem clone() throws CloneNotSupportedException {
        GenericItem clone = (GenericItem) super.clone();
        if (analogue != null) {
            clone.setAnalogue(analogue.clone());
        }
        return clone;
    }

    @Override
    public String toString() {
        return "GenericItem{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", analogue=" + analogue +
                ", category=" + category +
                '}';
    }
}
