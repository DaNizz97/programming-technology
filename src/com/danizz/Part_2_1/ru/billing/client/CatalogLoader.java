package com.danizz.Part_2_1.ru.billing.client;

import com.danizz.Part_2_1.ru.billing.stocklist.ItemCatalog;

public interface CatalogLoader {
    void load(ItemCatalog catalog);
}
