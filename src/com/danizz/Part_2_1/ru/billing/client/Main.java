package com.danizz.Part_2_1.ru.billing.client;

import com.danizz.Part_2_1.ru.billing.stocklist.*;

import java.util.Arrays;
import java.util.Date;

public class Main {

    public static void main(String[] args) throws CloneNotSupportedException {
        GenericItem genericItem1 = new GenericItem();
        genericItem1.setName("First Name");
        genericItem1.setPrice(9312.21F);
        GenericItem genericItem2 = new GenericItem();
        genericItem2.setName("Second Name");
        genericItem2.setPrice(53223.21F);
        GenericItem genericItem3 = new GenericItem();
        genericItem3.setName("Third Name");
        genericItem3.setPrice(859302.22F);
        GenericItem genericItem4 = new GenericItem();
        genericItem4.setID(2);
        genericItem4.setName("Third Name");
        genericItem4.setPrice(859302.22F);

        genericItem1.printAll();
        genericItem2.printAll();
        genericItem3.printAll();
        genericItem3.setAnalogue(genericItem4);
        genericItem3.getAnalogue().printAll();

        GenericItem[] items = new GenericItem[2];
        items[0] = new FoodItem("food");
        items[1] = new TechnicalItem();

        items[0].setCategory(Category.FOOD);
        items[0].setName("Food");
        items[0].setPrice(11F);

        items[1].setName("Tech");
        items[1].setPrice(334F);

        GenericItem ggg = new GenericItem();
        GenericItem fff = new FoodItem("Kek");

        ggg = fff;

        Arrays.stream(items).forEach(GenericItem::printAll);

        if (!ggg.equals(fff)) {
            try {
                throw new Exception("not equals");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        GenericItem clone = genericItem3.clone();
        clone.getAnalogue().setName( "sssss");
        genericItem3.getAnalogue().printAll();
        clone.getAnalogue().printAll();
        System.out.println(clone);

        /*
        Lab 3
         */
        String toSplit = "Конфеты ’Маска’;45;120";
        String[] itemFields;
        itemFields = toSplit.split(";");

        FoodItem foodItem = new FoodItem(itemFields[0]);
        foodItem.setPrice(Float.parseFloat(itemFields[1]));
        foodItem.setExpires(Short.parseShort(itemFields[2]));

        foodItem.printAll();

        /*
        Lab 4
         */

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=");
        ItemCatalog catalog = new ItemCatalog();
        catalog.addItem(genericItem1);
        catalog.addItem(genericItem2);
        catalog.addItem(genericItem3);
        catalog.addItem(genericItem4);
        catalog.addItem(new FoodItem("Rere"));
        catalog.addItem(new FoodItem("Uteo"));
        catalog.addItem(new GenericItem());
        catalog.addItem(new TechnicalItem());
        catalog.addItem(new TechnicalItem());
        catalog.addItem(new FoodItem("FOOOOOD"));

        long begin = new Date().getTime();
        for (int i = 0; i < 100000; i++) catalog.findItemByID(2);
        long end = new Date().getTime();
        System.out.println("In HashMap: " + (end - begin));
        begin = new Date().getTime();
        for (int i = 0; i < 100000; i++) catalog.findItemByIDAL(2);
        end = new Date().getTime();
        System.out.println("In ArrayList: " + (end - begin));

        CatalogLoader loader = new CatalogStubLoader();
        loader.load(catalog);
        catalog.printItems();
    }
}

