package com.danizz.Part_1_3;

import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
//        printAlphabet();
//        printAlphabetByStream();
        findMax();
        transposeMatrix(3);
    }

    private static void printAlphabet() {
        for (char i = 'a'; i <= 'z'; i++) {
            System.out.println(i);
        }
    }

    private static void printAlphabetByStream() {
        IntStream.range('a', 'z' + 1).forEach(i -> System.out.println((char) i));
    }

    private static void timeMeasurement() {
        long begin = new java.util.Date().getTime();
        int i = 0;
        for (i = 0; i < 100000000;) {
            i++;
        }
        long end = new java.util.Date().getTime();
        System.out.println(end - begin);
        System.out.println(i);
    }

    private static void findMax() {
        int[] mas = {12,43,12,-65,778,123,32,76};
        int max = Integer.MIN_VALUE;
        for (int ma : mas) {
            if (max < ma) {
                max = ma;
            }
        }
        System.out.println(max);
    }
    private  static void transposeMatrix(int n) {
        int[][] matrix = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j]=(int)Math.round(Math.random()*10);
            }
        }
        System.out.println("Начальная матрица");
        printMatrix(matrix);
        for (int i = 0; i < n; i++) {
            for (int j = i+1; j < n; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = temp;
            }
        }
        System.out.println("\nТранспонированная матрица");
        printMatrix(matrix);

    }
    private static void printMatrix(int[][] matrix) {
        System.out.println("------");
        for (int[] aMatrix : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.printf("%4d", aMatrix[j]);
            }
            System.out.println();
        }
    }
}
