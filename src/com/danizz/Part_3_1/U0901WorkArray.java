package com.danizz.Part_3_1;

public class U0901WorkArray<T extends Number> {
    public T[] arrNums;

    public U0901WorkArray(T[] arrNums) {
        this.arrNums = arrNums;
    }

    public double sum() {
        double doubleWork = 0;
        for (T arrNum : arrNums) {
            doubleWork += arrNum.doubleValue();
        }
        return doubleWork;
    }
}
