package com.danizz.Part_3_1;

public class U0901Main {

    public static void main(String[] args) {
        Integer intArr[] = {10, 20, 15};
        Float floatArray[] = new Float[3];
        for (int i = 0; i < floatArray.length; i++) {
            floatArray[i] = (float) (Math.random() * 50);
        }

        U0901WorkArray<Integer> insWorkArrayInt = new U0901WorkArray<>(intArr);
        U0901WorkArray<Float> insWorkArrayFloat = new U0901WorkArray<>(floatArray);

        System.out.println("Sum for intArray = " + insWorkArrayInt.sum());
        System.out.println("Sum for floatArray = " + insWorkArrayFloat.sum());
    }
}
